#!/bin/bash

# Quick script to launch startup programs
#set -e
set -o pipefail
#set -x

# define help menu
help_menu() {
	printf "%s USAGE:
--main, main programs
--email, email.
--vm, vm.
--media, media.
--all, all programs.\n" "${0}"
	exit 1
}

# Set flags
while [[ "${#}" -gt 0 ]]
do
	KEY="${1}"
	case "${KEY}" in
		--main)
			MAIN_FLAG=true
			shift 1
		;;

		--games)
			GAMES_FLAG=true
			shift 1
		;;

		--email)
			EMAIL_FLAG=true
			shift 1
		;;

		--vm)
			VM_FLAG=true
			shift 1
		;;

		--media)
			MEDIA_FLAG=true
			shift 1
		;;

		--all) 
			MAIN_FLAG=true
			EMAIL_FLAG=true
			VM_FLAG=true
			MEDIA_FLAG=true
			shift 1
		;;

		-h|--help)
			help_menu
			shift 1
		;;

		*)
			printf "%s invalid option.\n" "${KEY}"
			help_menu
		;;
	esac
done

# Main
main() {
    telegram-desktop &
    signal-desktop &
    element-desktop &
    transmission-qt &
    torbrowser-launcher &
}

# Games
games() {
	steam &
	multimc-bin &
}

# Emaail
email() {
    electron-mail &
    tutanota-desktop &
}

# VM 
vm() {
    virt-manager &
}

# Media
media() {
    steam &
    discord & 
    multimc &
    lutris &
    electrum &
}

# Run main flag if set true
if [ "${MAIN_FLAG}" == true ]
then
	main
fi

# Games
if [ "${GAMES_FLAG}" == true ]
then
	games
fi

# Run main flag if set true
if [ "${EMAIL_FLAG}" == true ]
then
	email
fi

# Run vm flag if set true
if [ "${VM_FLAG}" == true ]
then
	vm
fi

# Run media flag if set true
if [ "${MEDIA_FLAG}" == true ]
then
	main
fi
