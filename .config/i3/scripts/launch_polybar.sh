#!/bin/bash

#set -e
set -o pipefail
#set -x

# Script to launch polybar
POLYBAR_CONFIG="/home/insight/.config/polybar/config"
BAR="main"

# Get default interface
export DEFAULT_NETWORK_INTERFACE=$(ip route | grep -E '^default' | awk '{ print $5 }' | head -1)

# Test for config
if [ ! -e "${POLYBAR_CONFIG}" ]
then
    printf "%s not found.\n" "${POLYBAR_CONFIG}"
    exit 1
fi

# Killall old polybar instances
killall polybar

# Iterate through xrandr output and display monitor
for i in $(xrandr | grep -E " connected" | awk -F ' ' '{ print $1 }')
do
    MONITOR="${i}" polybar -c "${POLYBAR_CONFIG}" --reload "${BAR}" &
done
