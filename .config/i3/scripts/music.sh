#!/bin/bash

#set -e
set -o pipefail
#set -x

# define help_menu
function help_menu {
	printf "%s USAGE:
--command <Action>. Valid: play, pause\n"
	exit 1
}

while [[ "${#}" -gt 0 ]]
do
	KEY="${1}"
	case "${KEY}" in
		--comand)
			COMMAND="${2}"
			shift 2
		;;

		-h|--help)
			help_menu 
			shift 1
		;;

		*)
			printf "%s Invalid option.\n" "${KEY}"
			help_menu
			shift 1
		;;
	esac
done

# parse commands
case "${COMMAND}" in
	"pause")
		mpv pause 
		notify-send "Music pause."
	;;

	"play")
		mpc play
		notify-send "Music playing."
	;;

	*)
		printf "Invalid command.\n"
		help_menu
	;;
esac
