#!/bin/bash

# script to install main programs for i3 profile to render.
#set -e
set -o pipefail
#set -x

PACMAN_FILE="/usr/bin/pacman"
AUR_FILE="/usr/bin/aur"
APT_FILE="/usr/bin/apt-get"

# Lists
BASE_LIST=("i3" "feh" "udiskie" "kitty" "flameshot" "i3" "i3-gaps")

if [ -e "${PACMAN_FILE}" ]
then
	printf "Installing packages with pacman.\n"
	sudo pacman -Syu i3 feh udiskie kitty flameshot i3 i3-gaps

	if [ ! -e "${AUR_FILE}" ]
	then
		"${AUR_FILE}" polybar coindesk
	fi
elif [ -e "${APT_FILE}" ]
then
	printf "Installing packages with apt-get.\n"
	sudo ${APT_FILE} -y update
	sudo ${APT_FILE} -y install i3 polyabr udiskie kitty
else
	printf "No valid package manager."
	exit 1
fi
