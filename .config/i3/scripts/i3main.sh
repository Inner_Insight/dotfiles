#!/bin/bash

#set -e
set -o pipefail
#set -x

COMMAND="${1}"

function help_menu {
    printf "Usage %s lock, logout, suspend, hibernate, reboot, shutdown, i3-restart, i3-reload\n" "${0}"
}

case "${COMMAND}" in
    lock)
        printf "locking\n"
        xscreensaver-command --lock
    ;;

    logout)
        printf "logging out\n"
        i3-msg exit
    ;;

    suspend)
        printf "Suspending\n"
        systemctl suspend
    ;;

    hibernate)
        printf "hibernating\n"
        systemctl hibernate
    ;;

    reboot)
        printf "Rebooting\n"
        systemctl reboot
    ;;

    i3-restart)
        printf "Restarting i3\n"
        i3-msg restart
    ;;

    i3-reload)
        printf "Reloading i3\n"
        i3-msg reload
    ;;

    shutdown)
        printf "Shutting down\n"
        systemctl shutdown
    ;;

    *)
        printf "%s inalid\n" "${COMMAND}"
        help_menu
    ;;
esac
