#!/usr/bin/python3

import argparse
import subprocess
import os
import logging
# import re

logging.basicConfig(format="%(asctime)s %(message)s", level=logging.DEBUG)

# Define main argument objects
parser = argparse.ArgumentParser(description="Window function script.")
parser.add_argument("-c", "--command", help="Valid commands: reload, restart, \
logout, poweroff, reboot, suspend, lock")
parser.add_argument("-p", "--prompt", action="store_true", help="")

args = parser.parse_args()
commandArg = args.command
promptArg = args.prompt

# Prompt argument
if promptArg:
    promptArg = True
else:
    promptArg = False

# command argument
if commandArg:
    # Reload i3
    if commandArg == "reload":
        command = ["i3-msg", "reload"]
        logging.debug("command: %s", command)

    # Restart i3
    elif commandArg == "restart":
        command = ["i3-msg", "restart"]
        logging.debug("command: %s", command)

    # Quit i3
    elif commandArg == "logout":
        command = ["i3-msg", "exit"]
        logging.debug("command: %s", command)

    # Power off machine
    elif commandArg == "poweroff":
        command = ["sudo", "poweroff"]
        logging.debug("command: %s", command)

    # Reboot machine
    elif commandArg == "reboot":
        command = ["sudo", "reboot"]
        logging.debug("command: %s", command)

    # Suspend machine
    elif commandArg == "suspend":
        command = ["sudo", "pm-suspend"]
        logging.debug("command: %s", command)

    # Lock machine
    elif commandArg == "lock":
        command = ["xscreensaver-command", "--lock"]
        logging.debug("command: %s", command)

    else:
        print("Invalid Command.")
        logging.critical("Invalid command.")
        exit(1)
else:
    print("Nothing set for COMMAND.")
    exit(1)


# Prompt argument

# Goal: Create a basic GUI window with yes or no buttons.
def prompt_window(command):
    print(command)
    user_input = input("Run? ")
    logging.debug("user_input: %s", user_input)

    if user_input == "y":
        print("Running:", command)
        subprocess.call(command, shell=False)
    else:
        print("Skipping")

    # Launch with prompt window


# Run command
try:
    command
    if promptArg:
        logging.debug("promptArg: %s", promptArg)
        logging.debug("command: %s", command)
        prompt_window(command)
    else:
        logging.debug("command: %s", command)
        subprocess.call(command, shell=False)
except KeyError:
    print("Nothing to run for COMMAND.")
    exit(1)
