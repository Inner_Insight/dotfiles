#!/bin/bash

set -e
set -o pipefail
set -x

# Simple script to launch terminal depending on system's hostname.
KITTY_TERM="/usr/bin/kitty"
RXVT_TERM="/usr/bin/rxvt"

# Define main terminal
MAIN_TERM=""

# Define hostname excemptions for low powered systems. pi00 and pi01 in this case.
case "${HOSTNAME}" in
	"pi00"|"pi01")
		MAIN_TERM="${RXVT_TERM}"
	;;

	# Default to else
	*)
		MAIN_TERM="${KITTY_TERM}"
		if [ ! -f "${MAIN_TERM}" ]
		then
			printf "%s not found. Cannot run.\n"
			exit 1
		fi

		# run terminal and arguments.
		"${MAIN_TERM}" "${@}"
	;;
esac
