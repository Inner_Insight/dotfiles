#!/bin/bash

#set -e
set -o pipefail
#set -x

# copy .fehbg set file to i3 wallpapers
I3_WALLPAPER_DIR="/home/insight/.config/i3/wallpapers"
if [ ! -d "${I3_WALLPAPER_DIR}" ]
then
	printf "%s does not exist.\n" "${I3_WALLPAPER_DIR}"
	exit 1
fi

# Set file to read
FEHBG_FILE="/home/insight/.fehbg"
if [ ! -f "${FEHBG_FILE}" ]
then
	printf "%s not found.\n" "${FEHBG_FILE}"
	exit 1
fi

# Get current wallpaper set under /home/insight/.fehbg
CURRENT_WALLPAPER="$(cat "${FEHBG_FILE}" | tail -1 | awk -F ' ' '{ print $NF }' | sed "s/'//g")"

# Copy current wallpaper to i3 wallpaper directory
printf "Copying %s to %s\n" "${CURRENT_WALLPAPER}" "${I3_WALLPAPER_DIR}"
cp "${CURRENT_WALLPAPER}" "${I3_WALLPAPER_DIR}"
