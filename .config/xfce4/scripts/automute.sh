#!/bin/bash

#set -e
set -o pipefail
#set -x

# amixer set "Auto-Mute Mode" Enabled
# amixer set "Auto-Mute Mode" Disabled

# Script to automute toggle headphones.
CONTROL="Auto-Mute Mode"
CONTROL_STATUS=$(amixer sget "${CONTROL}" | grep -E "Item0" | awk -F ' ' '{ print $2 }' | sed "s/'//g")

# Figure out baised on output
case "${CONTROL_STATUS}" in
	"Enabled")
		echo "Disabling ${CONTROL}"
		amixer set "${CONTROL}" Disabled 2>&1>/dev/null
	 ;;

	"Disabled")
		echo "Enabling ${CONTROL}"		
		amixer set "${CONTROL}" Enabled 2>&1>/dev/null
	;;
	
	*)
	   echo "Invalid status, nothing to do."
	   exit 1
	;;
esac
