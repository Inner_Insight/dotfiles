#!/bin/bash

# Main colors

# Range colors
TXT_GREEN="%{F#22ff00}"
TXT_YELLOW="%{F#ffee00}"
TXT_RED="%{F#ff0000}"

# Highlight text color
TXT_HL="%{F#5294e2}"

# Normal font color
TXT_FG="%{F#FFFFFF}"

# Normal font bg color
TXT_BG="%{F#FFFFFF}"

# Normal background color
BG_FG="%{B#383c4a}"

# Normal background bg color
BG_BG="%{B#383c4a}"

# Delay between loops
LOOP_DELAY=1

# Main variable to set for colored output
COLOR=true

# Requirements: acpi, bc, awk

# %{l} - left leaning on text
# %{r} - right leaning on text
# %{c} - center leaning on text

#echo "%{c}%{F#FFFF00}%{B#0000FF} $(Clock) %{F-}%{B-}"
#echo "%{c}%{F#FFFF00}%{B#0000FF} Test %{F-}%{B-}"

function workspace {
	# i3 info
}

function sys_info {
	# System Info
	SYS_INFO="${TXT_HL}${HOSTNAME}%{F-} $(uname -r)"
}

function sys_mpd {
	# MPD
	MPD_SONG=$(mpc | head -1)
	MPD_PLAY_STATUS=$(mpc | grep -E '\[|\]' | awk -F ' ' '{ print $1 }' | sed 's/\[//g; s/\]//g')
	MPD_PLAY_TIME=$(mpc | grep -E '\[|\]' | awk -F ' ' '{ print $3 }')
	MPD_STATUS="${TXT_HL}MPD:%{F-} ${MPD_PLAY_STATUS} ${MPD_SONG} ${MPD_PLAY_TIME}"
}

function sys_mem {
	# Memory
	MEM_SED=$(free -h | grep -E "Mem:" | awk -F ' ' '{ print $3 }')
	MEM_TOTAL=$(free -h | grep -E "Mem:" | awk -F ' ' '{ print $2 }')
	
	MEM_SWAP_USED=$(free -h | grep -E "Swap:" | awk -F ' ' '{ print $3 }')
	MEM_SWAP_TOTAL=$(free -h | grep -E "Swap:" | awk -F ' ' '{ print $2 }')
	
	# Raw stats for percentages
	RAW_MEM_MAIN_USED=$(free | grep -E "Mem:" | awk -F ' ' '{ print $3 }')
	RAW_MEM_MAIN_TOTAL=$(free | grep -E "Mem:" | awk -F ' ' '{ print $2 }')
	RAW_MEM_SWAP_USED=$(free | grep -E "Swap:" | awk -F ' ' '{ print $3 }')
	RAW_MEM_SWAP_TOTAL=$(free | grep -E "Swap:" | awk -F ' ' '{ print $2 }')

	# Raw numbers for accurate percent output 
	MEM_MAIN_PERC="$(echo "scale=2; ${RAW_MEM_MAIN_USED} / ${RAW_MEM_MAIN_TOTAL}" | bc | sed 's/^\.//g')"
    MEM_SWAP_PERC="$(echo "scale=2; ${RAW_MEM_SWAP_USED} / ${RAW_MEM_SWAP_TOTAL}" | bc | sed 's/^\.//g')"

	# Test MEM_MAIN_PERC
	MEM_MAIN_PERC_NUM=$(echo "${MEM_MAIN_PERC}")

	# Memory color
	if [ ${COLOR} == true ]
	then
		# Small usage for: main
		if [ ${MEM_MAIN_PERC_NUM} -lt 50 ]
		then
			MEM_MAIN_PERC="${MEM_MAIN_PERC}%%%{F-}"
	
		# Yellow if above 50 percent
		elif [ ${MEM_MAIN_PERC_NUM} -gt 50 ]
		then
			MEM_MAIN_PERC="${TXT_YELLOW}${MEM_MAIN_PERC}%%%{F-}"
	
		# Red if above 80 percent
		elif [ ${MEM_MAIN_PERC_NUM} -gt 80 ]
		then
			MEM_MAIN_PERC="${TXT_RED}${MEM_MAIN_PERC}%%%{F-}"
		fi
	
	else
		MEM_MAIN_PERC="${MEM_MAIN_PERC}%"
	fi

	# Swap color
	if [ ${COLOR} == true ]
	then	
		# Swap color
		MEM_SWAP_PERC_NUM="${MEM_SWAP_PERC}"
	
		# Small usage for: swap
		if [ ${MEM_SWAP_PERC_NUM} -lt 50 ]
		then
			MEM_SWAP_PERC="${MEM_SWAP_PERC}%%%{F-}"
	
		# Yellow if above 50 percent
		elif [ ${MEM_SWAP_PERC_NUM} -gt 50 ]
		then
			MEM_SWAP_PERC="${TXT_YELLOW}${MEM_SWAP_PERC}%%%{F-}"
	
		# Red if above 80 percent
		elif [ ${MEM_SWAP_PERC_NUM} -gt 80 ]
		then
			MEM_SWAP_PERC="${TXT_RED}${MEM_SWAP_PERC}%%%{F-}"
		fi
	else
		MEM_SWAP_PERC="${MEM_SWAP_PERC}%"
	fi
	
	# Percentages only
	MEM_STATUS="${TXT_HL}Mem:%{F-} ${MEM_MAIN_PERC} ${TXT_HL}Swap:%{F-} ${MEM_SWAP_PERC}"
	
	# Full output
	#MEM_STATUS="${TXT_HL}Mem:%{F-} ${FREE_MEM_MAIN_USED} of ${FREE_MEM_MAIN_TOTAL} ${TXT_HL}Swap:%{F-} ${FREE_MEM_SWAP_USED} of ${FREE_MEM_SWAP_TOTAL}"
}

# Function to print date
function sys_date {
	# Clock format
	DATE_FORMAT="%a %b %m/%d/%Y %I:%M %p"

	DATE_STATUS="$(date "+${DATE_FORMAT}")"
}

# Function to check mount points 
function check_mount {
	USER_MOUNT="${1}"

	CHECK_MOUNT=$(mount | grep "${USER_MOUNT}" | wc -l)

	if [ ${CHECK_MOUNT} -gt 0 ]
	then
		return 0
	else
		return 1
	fi
}

# Function to print storage info 
function sys_storage {
	# Root
	check_mount "/"
	if [ "${?}" -eq 0 ]
	then
		HD_ROOT="/"
		HD_ROOT_USED=$(df -h "${HD_ROOT}" | tail -1 | awk -F ' ' '{ print $3 }')
		HD_ROOT_TOTAL=$(df -h "${HD_ROOT}" | tail -1 | awk -F ' ' '{ print $2 }')
		HD_ROOT_PERC=$(df -h "${HD_ROOT}" | tail -1 | awk -F ' ' '{ print $5 }')

		# Percentages only
		STORAGE_STATUS="${TXT_HL}${HD_ROOT}:%{F-} ${HD_ROOT_PERC}"
	fi

	# /boot directory
	check_mount "/boot"
	if [ "${?}" -eq 0 ]
	then
		HD_BOOT="/boot"
		HD_BOOT_USED=$(df -h "${HD_BOOT}" | tail -1 | awk -F ' ' '{ print $3 }')
		HD_BOOT_TOTAL=$(df -h "${HD_BOOT}" | tail -1 | awk -F ' ' '{ print $2 }')
		HD_BOOT_PERC=$(df -h "${HD_BOOT}" | tail -1 | awk -F ' ' '{ print $5 }')

		# Percentages only
		STORAGE_STATUS="${STORAGE_STATUS} ${TXT_HL}${HD_BOOT}:%{F-} ${HD_BOOT_PERC}"
	fi
	
	# /mnt/data directory
	check_mount "/mnt/data"
	if [ "${?}" -eq 0 ]
	then
		HD_DATA="/mnt/data"
		HD_DATA_USED=$(df -h "${HD_DATA}" | tail -1 | awk -F ' ' '{ print $3 }')
		HD_DATA_TOTAL=$(df -h "${HD_DATA}" | tail -1 | awk -F ' ' '{ print $2 }')
		HD_DATA_PERC=$(df -h "${HD_DATA}" | tail -1 | awk -F ' ' '{ print $5 }')

		# Percentages only
		STORAGE_STATUS="${STORAGE_STATUS} ${TXT_HL}${HD_DATA}:%{F-} ${HD_DATA_PERC}"
	fi
	
	# /mnt/media directory
	check_mount "/mnt/media"
	if [ "${?}" -eq 0 ]
	then
		HD_MEDIA="/mnt/media"
		HD_MEDIA_USED=$(df -h "${HD_MEDIA}" | tail -1 | awk -F ' ' '{ print $3 }')
		HD_MEDIA_TOTAL=$(df -h "${HD_MEDIA}" | tail -1 | awk -F ' ' '{ print $2 }')
		HD_MEDIA_PERC=$(df -h "${HD_MEDIA}" | tail -1 | awk -F ' ' '{ print $5 }')

		# Percentages only
		STORAGE_STATUS="${STORAGE_STATUS} ${TXT_HL}${HD_MEDIA}:%{F-} ${HD_MEDIA_PERC}"
	fi
	
	# /mnt/windows directory
	check_mount "/mnt/windows"
	if [ "${?}" -eq 0 ]
	then
		HD_WINDOWS="/mnt/windows"
		HD_WINDOWS_USED=$(df -h "${HD_WINDOWS}" | tail -1 | awk -F ' ' '{ print $3 }')
		HD_WINDOWS_TOTAL=$(df -h "${HD_WINDOWS}" | tail -1 | awk -F ' ' '{ print $2 }')
		HD_WINDOWS_PERC=$(df -h "${HD_WINDOWS}" | tail -1 | awk -F ' ' '{ print $5 }')

		# Percentages only
		STORAGE_STATUS="${STORAGE_STATUS} ${TXT_HL}${HD_WINDOWS}:%{F-} ${HD_WINDOWS_PERC}"
	fi
}

# CPU info
function sys_cpu {
	# Display load averages from the past 1, 5 and 15 minutes
	#CPU_LOAD=$(uptime | grep -E -o "load average\:.*" | sed 's/load\ average\: //g')
	#CPU_STATUS="${TXT_HL}CPU:%{F-} ${CPU_LOAD}"

	#nproc

	# CPU Usage Percentage (Using vmstat)
	CPU_USAGE_NUM=$(( 100 - $(vmstat 1 2 | tail -1 | awk '{ print $15 }') ))

	if [ ${COLOR} == true ]
	then
		# Low tier
		if [ ${CPU_USAGE_NUM} -lt 50 ] 
		then
			CPU_STATUS="${TXT_HL}CPU:%{F-} ${CPU_USAGE_NUM}%%%{F-}"
	
		# Mid tier
		elif [ ${CPU_USAGE_NUM} -eq 50 ] || [ ${CPU_USAGE_NUM} -gt 50 ] 
		then
			CPU_STATUS="${TXT_HL}CPU:%{F-} ${TXT_YELLOW}${CPU_USAGE_NUM}%%%{F-}"

		# High tier
		elif [ ${CPU_USAGE_NUM} -eq 80 ] || [ ${CPU_USAGE_NUM} -gt 80 ] 
		then
			CPU_STATUS="${TXT_HL}CPU:%{F-} ${TXT_RED}${CPU_USAGE_NUM}%%%{F-}"
		fi

	else
		CPU_STATUS="${TXT_HL}CPU:%{F-} ${CPU_USAGE_NUM}%%"
	fi

	# CPU Temp
	#CPU_TEMP_NUM=$(sensors | grep -E "high|critical" | awk -F ' ' '{ print $3 }' | sed 's/^+//g'
}

function sys_battery {
	# BUG: Output still showing if device is not support.

	BAT_CHECK=$(acpi -b 2>&1 | grep -E "No support" | wc -l)
	if [ ${BAT_CHECK} -gt 0 ]
	then
		BAT_STATUS="N/A"
	else
	    BAT_STAT=$(acpi -b 2>&1 | awk -F ' ' '{ print $3 }' | sed 's/,//g')                    # status
	    BAT_PERC=$(acpi -b 2>&1 | awk -F ' ' '{ print $4 }' | sed 's/,//g; s/\%//g')           # Battery percentage
		BAT_TIME=$(acpi -b 2>&1 | awk -F ',' '{ print $3 }' | sed 's/^ //g')                   # Battery time remaining
		BAT_THRES=40
	
		if [ ! -z "${BAT_STAT}" ] && [ ! -z "${BAT_PERC}" ] && [ ! -z "${BAT_TIME}" ]
		then
			if [ ${COLOR} == true ]
			then
				# Check the percentage of battery and change text when in red.
				# Possible BUG
				if [ ${BAT_PERC} -lt ${BAT_THRES} ]
				then
					BAT_PERC="${TXT_RED}${BAT_PERC}%%%{F-}"

				# Near full or full
				elif [ ${BAT_PERC} -gt 90 ]
				then
					BAT_PERC="${TXT_GREEN}${BAT_PERC}%%%{F-}"

				else
					BAT_PERC="${BAT_PERC}%%"
				fi

				# Check battery time
				BAT_TIME_CHECK=$(echo "${BAT_TIME}" | grep -E "rate information unavailable" | wc -l)

				if [ ${BAT_TIME_CHECK} -gt 0 ]
				then
					BAT_TIME="N/A"
				fi
	
				BAT_STATUS="${TXT_HL}Bat:%{F-} ${BAT_STAT} ${BAT_PERC} ${BAT_TIME}"
			else
				BAT_STATUS="${TXT_HL}Bat:%{F-} ${BAT_STAT} ${BAT_PERC} ${BAT_TIME}"
			fi
		else
			BAT_STATUS="N/A"
		fi
	fi
}

function sys_network {
	# Device to use to parse output
	NET_INF="wlan0"
	NET_INF_CHECK=$(ip addr show ${NET_INF} 2>/dev/null | wc -l)

	# Check for the interface before running.
	if [ ${NET_INF_CHECK} -gt 0 ]
	then
		NET_IP=$(ip addr show ${NET_INF} | grep -E inet | awk -F ' ' '{ print $2 }' | head -1 | sed 's/\/24//g')
		WIRELESS_NET_SSID=$(iwconfig ${NET_INF} | grep -E "ESSID" | awk -F ' ' '{ print $4 }' | sed 's/ESSID\://g; s/\"//g')

		# Test if WIRELESS_NET_SSID is set before adding to NET_STATUS
		if [ ! -z "${WIRELESS_NET_SSID}" ]
		then
			WIRELESS_NET_SSID="${WIRELESS_NET_SSID}" 
		else
			WIRELESS_NET_SSID=""
		fi

		if [ ! -z "${NET_IP}" ]
		then
			# Full output
			#NET_STATUS="${TXT_HL}${NET_INF}: %{F-}${WIRELESS_NET_SSID} ${NET_IP}"

			# Short output 
			NET_STATUS="${TXT_HL}${NET_INF}: %{F-}${NET_IP}%{F-}" 
		else
			# If nothing is found
			NET_STATUS="N/A" 
		fi
	else
		NET_STATUS="N/A" 
	fi
}

function sys_volume { 
	VOL_CHANNEL="Master"
	VOL_STATUS=$(amixer sget ${VOL_CHANNEL} 2>/dev/null | grep -E '\[on\]' | wc -l)

	# Volume is muted
	if [ ${VOL_STATUS} -eq 0 ]
	then
	   # Show if volume is muted.
       VOL_STATUS="${TXT_HL}V:%{F-} ${TXT_RED}MUTED%{F-}"

	# Volume is not muted.
    elif [ ${VOL_STATUS} -gt 0 ]
	then 
	   # Show volume percent
	   VOL_PERC=$(amixer sget ${VOL_CHANNEL} 2>/dev/null | grep -E "\[on\]" | grep -E -o '\[.*%\]' | sed 's/\[//g; s/\]//g')
	   if [ ! -z ${VOL_PERC} ] 
	   then
       		VOL_STATUS="${TXT_HL}V:%{F-} ${VOL_PERC}%"   
       else
       		VOL_STATUS="${TXT_HL}V:%{F-} N/A"
		fi
	else
	   # If nothing is found.	
       VOL_STATUS="${TXT_HL}V:%{F-} N/A"
	fi
}

function sys_vpn {
	# Check for openvpn
	VPN_CHECK=$(ps -A | grep openvpn | wc -l)

	if [ ${VPN_CHECK} -gt 0 ]
	then
		VPN_STATUS="${TXT_HL}VPN:%{F-} C"
	else
		VPN_STATUS="${TXT_RED}VPN:%{F-} NC"
	fi
}

# Loop everything
while [ true ]
do
	# Info
	sys_info

	# CPU
	sys_cpu

	# MPD
	sys_mpd	

	# Memory
	sys_mem

	# HD
	sys_storage

	# Date
	sys_date

	# Battery
	sys_battery
	
	# Network
	sys_network

	# Volume
	sys_volume

	# VPN
	sys_vpn

	# Main string to be put out onto both monitors
	TXT_OUTPUT="%{l} ${TXT_FG}${BG_BG}${SYS_INFO} | ${CPU_STATUS} | ${MEM_STATUS} | ${STORAGE_STATUS} | ${BAT_STATUS} | ${NET_STATUS} | ${VOL_STATUS} | ${VPN_STATUS} %{r}${DATE_STATUS} %{F-}%{B-}"
	MON_COUNT=$(xrandr | grep " connected" | wc -l)

	if [ ${MON_COUNT} -gt 1 ]
	then
		# Output text onto both monitors
		echo -e "%{S0}${TXT_OUTPUT}%{S1}${TXT_OUTPUT}"
	else
		# Output text onto single monitor
		echo -e "${TXT_OUTPUT}"
	fi

	# Pause on each loop
	sleep ${LOOP_DELAY}
done
