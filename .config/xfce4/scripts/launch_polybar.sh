#!/bin/bash

# Script to launch polybar
POLYBAR_CONFIG="${HOME}/.config/polybar/config"
BAR="main"

if [ ! -e "${POLYBAR_CONFIG}" ]
then
    echo "${POLYBAR_CONFIG} not found."
    exit 1
fi

# Killall old polybar instances
killall polybar

# Iterate through xrandr output and display monitor
for i in $(xrandr | grep -E " connected" | awk -F ' ' '{ print $1 }')
do
    #MONITOR="${i}"
    MONITOR="${i}" polybar --reload "${BAR}" &
done
