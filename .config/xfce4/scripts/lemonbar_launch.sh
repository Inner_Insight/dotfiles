#!/bin/bash

PANEL_BG="#383c4a"
PANEL_FG="#FFFFFF"

# Launcher script for lemonbar within i3.
LEMONBAR_BIN="/usr/bin/lemonbar"

# Main script to render text into lemonbar
LEMONBAR_SCRIPT="/home/insight/.i3/lemonbar.sh"

# Main options for lemonbar
LEMONBAR_OPTS=()
LEMONBAR_OPTS+=("-p")
LEMONBAR_OPTS+=("-B ${PANEL_BG}")
LEMONBAR_OPTS+=("-F ${PANEL_FG}")


if [ -e "${LEMONBAR_BIN}" ] && [ "${LEMONBAR_SCRIPT}" ]
then
	"${LEMONBAR_SCRIPT}" | "${LEMONBAR_BIN}" "${LEMONBAR_OPTS[@]}"
else
	echo "Lmeonbar or ${LEMONBAR_SCRIPT} not found."
	exit 1
fi
