#!/bin/bash

#set -e 
set -o pipefail
set -x 

# Updated to original creators, modified for bash.

# Move the current window to the other vertical monitor. Adapted from the
# scripts for horizontal monitors at makandracards.com
#
# Only works on a 2x1 vertical monitor setup.
# Also works only on one X screen (which is the most common case).
#
# Props to
# http://icyrock.com/blog/2012/05/xubuntu-moving-windows-between-monitors/
# http://makandracards.com/makandra/12447-how-to-move-a-window-to-the-next-monitor-on-xfce-xubuntu
#
# Unfortunately, both "xdotool getwindowgeometry --shell $window_id" and
# checking "-geometry" of "xwininfo -id $window_id" are not sufficient, as
# the first command does not respect panel/decoration offsets and the second
# will sometimes give a "-0-0" geometry. This is why we resort to "xwininfo".

window_id="$(xdotool getactivewindow)"

screen_height="$(xdpyinfo | awk '/dimensions:/ { print $2 }' | cut -d 'x' -f 2)"
lower_display_height="$(xdotool getdisplaygeometry | cut -d " " -f 2)"
upper_display_height="$(( screen_height - lower_display_height ))"

# Remember if it was maximized.
window_state="$(xprop -id "${window_id}" _NET_WM_STATE | awk '{ print $3 }')"

# Un-maximize current window so that we can move it
wmctrl -ir "${window_id}" -b remove,maximized_vert,maximized_horz

# Read window position
x="$(xwininfo -id "${window_id}" | awk '/Absolute upper-left X:/ { print $4 }')"
y="$(xwininfo -id "${window_id}" | awk '/Absolute upper-left Y:/ { print $4 }')"

# Subtract any offsets caused by panels or window decorations
x_offset="$(xwininfo -id "${window_id}" | awk '/Relative upper-left X:/ { print $4 }')"
y_offset="$(xwininfo -id "${window_id}" | awk '/Relative upper-left Y:/ { print $4 }')"
x="$(( x - x_offset ))"
y="$(( y - y_offset ))"

# Compute new Y position
if [ "${y}" -gt "${upper_display_height}" ]; 
then
	new_y="$(( y - upper_display_height ))" # window on lower display -> substract for new position
else
	new_y="$(( y + upper_display_height ))" # window on upper display -> add for new position
fi

# We respect the window's height here: moving a window off more than half its height won't happen.
height="$(xdotool getwindowgeometry "${window_id}" | awk '/Geometry:/ { print $2 }' | cut -d 'x' -f 2)"
if [ $(( new_y + height / 2 )) -gt "${screen_height}" ] 
then
	new_y="$(( new_y - height ))"
fi

# Move the window
xdotool windowmove "${window_id}" "${x}" "${new_y}"

# Maximize window again, if it was before
if [ -n "${window_state}" ] 
then
  wmctrl -ir "${window_id}" -b add,maximized_vert,maximized_horz
fi
