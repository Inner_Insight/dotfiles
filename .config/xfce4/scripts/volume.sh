#!/bin/bash

#set -e
set -o pipefail
#set -x

# Simple script to change pulseaudio sound.

# Sinks to control
SINK_LIST=()

# Define help
function help_menu {
	printf "%s<FLAGS>
%s -s SINK1 -s SINK2 -s SINK3 control multiple sinks.
%s -c COMMAND. Issue command to sinks. Valid commands: raise, lower, mute, unmute\n" "${0}"
	exit 1
}

# Iterate over arguments
while [ "${#}" -gt 0 ]
do
	KEY="${1}"
	case "${KEY}" in
		-s|--sink)
			SINK_LIST+=("${2}")
			shift 2
		;;

		-c|--comamnd)
			COMMAND="${2}"
			shift 2
		;;

		-h|--help)
			help_menu
			shift 1
		;;
	esac
done

# Check over arguments
if [ -z "${COMMAND}" ]
then
	printf "COMMAND not set.\n"
	help_menu
	exit 1
fi

if [ -z "${SINK_LIST[*]}" ]
then
	printf "Nothing for SINK_LIST\n"
	help_menu
	exit 1
fi

# Parse command
case "${COMMAND}" in
	# Raise sinks
	"raise")
		for SINK in "${SINK_LIST[@]}"
		do
			printf "Raising %s\n" "${SINK}"
			/usr/bin/pactl set-sink-volume "${SINK}" +5%
		done
	;;

	# Lower sinks
	"lower")
		for SINK in "${SINK_LIST[@]}"
		do
			printf "Lowering %s\n" "${SINK}"
			/usr/bin/pactl set-sink-volume "${SINK}" -5%
		done
	;;

	# Mute sinks
	"mute")	
		for SINK in "${SINK_LIST[@]}"
		do
			printf "Muting %s\n" "${SINK}"
			/usr/bin/pactl set-sink-mute "${SINK}" 1
		done
	;;

	# Unmute
	"unmute")
		for SINK in "${SINK_LIST[@]}"
		do
			printf "Unmuting %s\n" "${SINK}"
			/usr/bin/pactl set-sink-mute "${SINK}" 0
		done
	;;

	# Everything else
	*)
		printf "-c ${COMMAND} invalid command."
		help_menu
		exit 1
	;;
esac

