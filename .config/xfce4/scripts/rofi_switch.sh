#!/bin/bash

CONFIG="/home/insight/.config/rofi/custom.rasi"

if [ ! -e "${CONFIG}" ]
then
	echo "${CONFIG} not found."
	exit 1
fi

# Rofi example
#rofi -show run -font "Input Mono 14" -fg "#B0E670" -bg "#14161F" -hlfg "#14161F" -hlbg "#B0E670" -bc "#14161F" -location 0 -lines 19 -bw 0 -font "Input Mono Bold 16" -padding 10 -separator-style none -opacity 80 -hide-scrollbar -width 50% -height 30%

# Original with combining window switcher and run application.
#rofi -config ${CONFIG} -width ${WIDTH} -lines ${LINES} -modi run,drun -show combi -modi combi

rofi -config ${CONFIG} -width 60 -lines 10 -show window -padding 10 --seperator-style none -hide-scrollbar -font "Fantasque Sans Mono 12"

#rofi -config ${CONFIG} -width 60 -line 10 -show run -location 0 -bw 0 -padding 10 -seperator-style none -hide-scrollbar
#rofi -config ${CONFIG} -width 60 -lines 8 -modi run,drun -show combi -modi combi -padding 10 -seperator-style none -hide-scrollbar -opacity 90 -dmenu -p ">"
