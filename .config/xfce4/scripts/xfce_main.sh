#!/bin/bash

#set -e
set -o pipefail
#set -x

COMMAND="${1}"

function help_menu {
    printf "Usage %s <COMMAND>
Valid commands: lock, logout, suspend, hibernate, reboot, shutdown\n" "${0}"
}

case "${COMMAND}" in
    lock)
        printf "locking\n"
        xflock --lock
    ;;

    logout)
        printf "logging out\n"
        xfce4-session-logout -logout -fast
    ;;

    suspend)
        printf "Suspending\n"
        xfce4-session-logout -suspend
    ;;

    hibernate)
        printf "hibernating\n"
        xfce4-session-logout -hibernate
    ;;

    reboot)
        printf "Rebooting\n"
        xfce4-session-logout -reboot -fast
    ;;

    shutdown)
        printf "Shutting down\n"
        xfce4-session-logout -halt -fast
    ;;

    *)
        printf "%s invalid\n" "${COMMAND}"
        help_menu
    ;;
esac
