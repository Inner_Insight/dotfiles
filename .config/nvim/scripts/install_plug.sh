#!/bin/bash

set -e
set -o pipefail
#set -x

# Simple script to install plug interface to vim

AUTOLOAD_DIR="${HOME}/.config/nvim/autoload"
PLUG_FILE="${AUTOLOAD_DIR}/plug.vim"

#curl -fLo "${PLUG_FILE}" --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
if [ ! -d "${AUTOLOAD_DIR}" ]
then
	printf "Creating %s\n" "${AUTOLOAD_DIR}"
	mkdir -p "${AUTOLOAD_DIR}"
fi

printf "Downloading plug.\n"
wget -c "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" -O "${PLUG_FILE}"

