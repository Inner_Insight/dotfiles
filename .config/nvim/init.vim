" Add in plug
call plug#begin('~/.config//nvim/plugged')
	" auto-complete
	Plug 'davidhalter/jedi-vim'
    
	" Fuzz finder
	Plug 'junegunn/fzf'

	" Syntax checking
	Plug 'dense-analysis/ale'

	" nerdtree
	Plug 'preservim/nerdtree'

	" Themes
	Plug 'morhetz/gruvbox'
	Plug 'dracula/vim'
	Plug 'arcticicestudio/nord-vim'
	Plug 'cocopon/iceberg.vim'
	Plug 'ajmwagar/vim-deus'
call plug#end()

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Set copy indent
set copyindent 

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
set visualbell           " don't beep
set noerrorbells         " don't beep

" Better command-line completion
set wildmenu
 
" Show partial commands in the last line of the screen
set showcmd
 
" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch

set incsearch

" Set colorscheme
colorscheme gruvbox

" Enable syntax highlighting
syntax on
syntax enable

" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
set tabstop=4
set shiftwidth=4
set shiftwidth=4
set softtabstop=4
"set expandtab
set noexpandtab

" Show numbers
set number

" Enable completion where available.
" This setting must be set before ALE is loaded.
"
" You should not turn this setting on if you wish to use ALE as a completion
" source for other completion plugins, like Deoplete.
" Enable completion where available.
" This setting must be set before ALE is loaded.
let g:ale_completion_enabled = 1

" Ale options
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {'python': ['flake8']}

