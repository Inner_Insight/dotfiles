# Load zsh functions
ZSH_FUNCTIONS="${HOME}/.zsh/functions"
ZSH_ALIASES="${HOME}/.zsh/aliases"

# ZSH_ALIASES
if [ -e "${ZSH_ALIASES}" ]
then
    . "${ZSH_ALIASES}"
else
    printf "Could not load %s file.\n" "${ZSH_ALIASES}"
fi

# ZSH_FUNCTIONS
if [ -e "${ZSH_FUNCTIONS}" ]
then
    . "${ZSH_FUNCTIONS}"
else
    printf "Could not load %s file.\n" "${ZSH_FUNCTIONS}"
fi

# Load custom_prompt
custom_prompt
